package com.gorodkov.dmitriy.testci

import com.gorodkov.dmitriy.testci.model.Calculator
import org.junit.Test

import org.junit.Assert.*
import org.junit.Before

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    lateinit var calculator: Calculator

    @Before
    @Throws
    fun createCalculator() {
        calculator = Calculator()
    }

    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }

    @Test
    fun plusTest() {
        assertEquals(3.0f, calculator.plus(1f, 2f))
    }

    @Test
    fun divideByZeroTest() {
        assertEquals(0f, calculator.divide(5f, 0f))
    }

    @Test
    fun divideTest() {
        assertEquals(5f, calculator.divide(10f, 2f))
    }

}
