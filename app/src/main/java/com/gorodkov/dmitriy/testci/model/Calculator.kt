package com.gorodkov.dmitriy.testci.model

class Calculator {
    fun plus(valueOne: Float, valueTwo: Float): Float {
        return valueOne + valueTwo
    }

    fun divide(valueOne: Float, valueTwo: Float): Float {
        return if (valueTwo == 0f) {
            0f
        } else {
            valueOne / valueTwo

        }

    }

}